-include .env
export

DAEMONIZE ?=
ifdef DAEMONIZE
DAEMONIZE_OPTION=-d
endif

define check_var_exists
	@if ! [ $(2) ]; then\
		echo "Error: $(1) is not set";\
		exit 1;\
	fi
endef

build-client: create-client
	cd ${PROJECT_NAME}/client && ls && docker build -t ${DOMAIN}/rsnapshot-host:1.0 --build-arg PASSWORD=${PASSWORD} --build-arg USER=${USER} --progress=plain .

create-env:
	@if ! [ -f .env ]; then\
		echo "No .env found. Adding from examples...";\
		cp examples/.env .;\
	fi

create-my-project:
	@$(call check_var_exists,PROJECT_NAME,${PROJECT_NAME})

	@if ! [ -d ${PROJECT_NAME} ]; then\
		echo "Making dir: ${PROJECT_NAME}";\
		mkdir -p ${PROJECT_NAME};\
	fi

create-server: create-my-project
	@if ! [ -d ${PROJECT_NAME}/server ]; then\
		echo "Making dir: ${PROJECT_NAME}/server";\
		mkdir -p ${PROJECT_NAME}/server;\
	fi

	@if ! [ -f ${PROJECT_NAME}/server/docker-compose.yml ]; then\
		echo "No ${PROJECT_NAME}/server/docker-compose.yml. Adding from examples...";\
		cp examples/server/docker-compose.yml ${PROJECT_NAME}/server;\
	fi

create-client: create-my-project
	@if ! [ -d ${PROJECT_NAME}/client ]; then\
		echo 'Creating ${PROJECT_NAME}/client dir';\
		mkdir ${PROJECT_NAME}/client;\
	fi

	@if ! [ -f ${PROJECT_NAME}/client/Dockerfile ]; then\
		echo "Copying Dockerfile to ${PROJECT_NAME}/client/";\
		cp files/Dockerfile ${PROJECT_NAME}/client/Dockerfile;\
	fi

	@if ! [ -f ${PROJECT_NAME}/client/docker-compose.yml ]; then\
		echo "No ${PROJECT_NAME}/client/docker-compose.yml. Adding from examples...";\
		cp examples/client/docker-compose.yml ${PROJECT_NAME}/client;\
	fi

	@if ! [ -f ${PROJECT_NAME}/client/entrypoint.sh ]; then\
		echo "No ${PROJECT_NAME}/client/entrypoint.sh. Adding from files...";\
		cp files/entrypoint.sh ${PROJECT_NAME}/client;\
	fi

	@if ! [ -d ${PROJECT_NAME}/client/config/wireguard ]; then\
		echo "Creating ${PROJECT_NAME}/client/config/wireguard dir";\
		mkdir -p ${PROJECT_NAME}/client/config/wireguard;\
	fi


create-server-rsnapshot: create-server
	@$(call check_var_exists,TZ,${TZ})

	@PROJECT_NAME=${PROJECT_NAME} TZ=${TZ} docker compose -f ${PROJECT_NAME}/server/docker-compose.yml run --rm -d rsnapshot
	sleep 1
	@PROJECT_NAME=${PROJECT_NAME} TZ=${TZ} docker compose -f ${PROJECT_NAME}/server/docker-compose.yml exec -it rsnapshot ssh-keygen
	@PROJECT_NAME=${PROJECT_NAME} TZ=${TZ} docker compose -f ${PROJECT_NAME}/server/docker-compose.yml stop rsnapshot

client-up:
	@$(call check_var_exists,DOMAIN,${DOMAIN})
	@$(call check_var_exists,TZ,${TZ})

	@if ! [ -f ${PROJECT_NAME}/client/config/wireguard/wg0.conf ]; then\
		echo "No wg0.conf found. See readme/Setting up Client";\
		exit 1;\
	fi
	DOMAIN=${DOMAIN} TZ=${TZ} docker compose -f ${PROJECT_NAME}/client/docker-compose.yml up ${DAEMONIZE_OPTION}


server-up: create-server
	@$(call check_var_exists,TZ,${TZ})
	@PROJECT_NAME=${PROJECT_NAME} TZ=${TZ} docker compose -f ${PROJECT_NAME}/server/docker-compose.yml up ${DAEMONIZE_OPTION}

server-setup-ssh:
	@$(call check_var_exists,TZ,${TZ})
	@$(call check_var_exists,USER,${USER})
	@$(call check_var_exists,CLIENT_IP,${CLIENT_IP})
	@PROJECT_NAME=${PROJECT_NAME} TZ=${TZ} docker compose -f ${PROJECT_NAME}/server/docker-compose.yml exec -it rsnapshot ssh-copy-id ${USER}@${CLIENT_IP}


