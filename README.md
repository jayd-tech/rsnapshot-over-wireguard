# Giving restricted, automated access to a known user

### In general, we want to solve the problem of losing code/data due to silly mistakes.

Common examples are as follows:

- A forgotten git stash results in a loss of work or heavy conflicts on pop
- A local database is malformed, and we want to recover its version from 30 minutes ago

### Specifically, we want these backups done remotely in order to preserve space and account for anything happening to mobile machines in transit.

These remote activities should be:

- Flexible: The server should be able to connect to the client so long as both have an internet connect, regardless of where each are.
- Automated: Backups should occur regularly and without user interaction.
- Secure: Transfers should be encrypted, in case either device is on a public network.

### To satisfy these specifications, we require the following:

- Private VPN (i.e., wireguard server and client): For a portable, encrypted, private network.
- Docker (on server and client): To reduce the attack vector exposed by the automated client/server communication.

## Setting up your project

- First, you must create a project directory in which your config files may live.
  To do this, run

  ```
  make create-env
  ```

  This will copy over an example env file to your
  root dir.

- Next, you must create a project directory by doing the following:

1. Populate the PROJECT_NAME var in the .env file of your root directory.
2. Run
   ```
   make create-my-project
   ```

## Setting up the server

We make use of docker-wireguard (https://github.com/linuxserver/docker-wireguard) to create and configure a wireguard server. A sample compose file is in the examples/server dir.

### Requirements

- A static ip or dynamic domain: If not specified in the service's environment variables (as SERVERURL), the host's external IP is combined with port 51820 (if not overriden with SERVERPORT) in order to generate the endpoint needed for the client to access the wireguard server.
- Exposed port: If the example compose file's ports config is unaltered, the default wireguard port will be forwarded from the host. If changing, also override the SERVERPORT env variable in the compose file.

### What happens

On initial startup, docker-wireguard will generate both the peer and server configuration files required for communication. If the example compose file's volumes remain unaltered, these files will be available on the host at ./wireguard/config.

### Instructions

- Run
  ```
  make create-server
  ```
  to generate a server_dir in your project, and then generate a compose file.
- Populate the TZ var in the .env file of your root directory.
- Alter the generated compose file as desired (https://github.com/linuxserver/docker-wireguard)
- Run
  ```
  make server-up
  ```

## Setting up the client

### Requirements

- Populate the USER and PASSWORD vars in the .env file of your root directory. These will be the
  user/password your wireguard server uses to ssh into your wireguard client to take snapshots.

- Because docker-wireguard does not include an ssh-server (which we need to automate direct docker wireguard client - docker wireguard server communication), run the following (in the root dir) to generate the image referenced in the sample/client/compose file.

```
make build-client
```

### Instructions

- Run

  ```
  make create-client
  ```

  If you've already run `make build-client`, this should result in a no-op.

- After following the server setup instructions, you can find one or more peer{n}.conf files
  in the {your-project}/server/wireguard/config/peer{i} dirs.

1. Copy a peer{i}.conf file into {your-project}/client/config/wireguard, and rename to wg0.conf.
2. Remove the ListenPort entry from under [Interface].
3. Remove the IPv6 range (`::/0`) from AllowedIps.

```
[Peer]
...
AllowedIPs = 0.0.0.0/0
```

4. Add `PostUp = ping -c1 {your_subdomain_DNS}` to the bottom of [Interface]. This will ensure
   your server is able to communicate with the client upon client startup.

```
[Interface]
...
PostUp = ping -c1 10.8.0.1.

```

- In your project's client folder's compose file, change the first part of /data:/data volume mount to the directory you wish to mount. e.g., `/home/$(whoami)/my_code_repo:/data`. If mounting multiple dirs, do so such that each is mounted as part of data.

```
services:
  wireguard:
  ...
  volumes:
    - /my_data_dir_1:/data/my_data_dir_1
    - /my_data_dir_2:/data/my_data_dir_2
```

## Setting up snapshots

- The server should backup snapshots to a persistent storage. In your project's server/docker-compose.yml file, override the /data:/.snapshots mapping with your desired storage.

```
rsnapshot:
  ...
  volumes:
    ...
    - /home/$(whoami)/my_permanent_dir:/.snapshots
```

- Run

  ```
  make create-server-rsnapshot
  ```

  to generate the required config files and ssh keys.

The server consumes one or more config files in {PROJECT_NAME}/server/rsnapshot/config"

- With the client running, run
  ```
  server-setup-ssh
  ```

